import React from "react";
import ReactDOM from "react-dom";

class Recommendations extends React.Component {
  render() {
    const imgStyle = {
      height: 192,
      display: "inline",
      marginRight: 8
    };
    return (
      <div
        class="ting-object-wrapper"
        style={{
          display: "block",
          "overflow-x": "scroll",
          "white-space": "nowrap",
          width: "100%"
        }}
      >
        <h3>Prøv også:</h3>
        {this.props.related.map(({ pid }) => {
          return (
            <a key={pid} href={`/ting/object/${pid}`}>
              <img
                style={imgStyle}
                src={`https://demo.bibspire.dk/bib/cover?cover=${pid}`}
              />
            </a>
          );
        })}
      </div>
    );
  }
}

const urlMatch = location.href.match(/\/ting\/object\/([^?#.]*)/);
if (urlMatch) {
}

let started = false;
async function attach() {
  if (started) {
    return;
  }
  const parent = document.querySelector(".ting-object");
  if (parent) {
    started = true;
    const elem = document.createElement("div");
    parent.appendChild(elem);

    const pid = urlMatch[1];
    let related = await fetch(
      "https://demo.bibspire.dk/api/recommend?one_per_creator=true&longtailedness=0.4&limit=20&pid=" +
        pid
    );
    related = await related.json();
    related = related && related.related;
    if (related) {
      ReactDOM.render(<Recommendations related={related} />, elem);
    }
  }
}
document.addEventListener("DOMContentLoaded", attach);
attach();
